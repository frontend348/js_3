"use strict"
//код из первого видео
let num = 20

function showFirstMessage(text) {
    console.log(text)
    console.log(num)
}

showFirstMessage('Hello World!')
console.log(num)

console.log(calc(4, 3))
console.log(calc(5, 6))
console.log(calc(10, 6))

function calc(a, b) {
    return a + b
}

function ret() {
    let num = 50

    return num
}

const anotherNum = ret()
console.log(anotherNum)

const logger = function () {
    console.log('Hello!')
}
logger()

const calc2 = (a, b) => a + b

const calc3 = (a, b) => {
    console.log('Welcome to Calc!')
    return a + b
}

//код из второго видео
const str = 'teSt'
console.log(str.toUpperCase())
console.log(str.toLowerCase())
console.log(str)

const fruit = 'Some fruit'
console.log(fruit.indexOf('fruit'))
console.log(fruit.indexOf('q'))

const logg = 'Hello world'
console.log(logg.slice(6, 10))
console.log(logg.slice(6))
console.log(logg.slice(-5, -1))

console.log(logg.substring(6, 11))
console.log(logg.substr(6, 5))

const num2 = 12.2
console.log(Math.round(num2))

const test = '12.2px'
console.log(parseInt(test))
console.log(parseFloat(test))
